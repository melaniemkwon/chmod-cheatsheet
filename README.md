```
### ownerships
u = User (you)
g = Group
o = Other (aka. 'World')
a = All of the above

### permission keywords
  = (blank) no permissions
x = Execute
w = Write
r = Read

### operators
+ Add
- Remove
= Equals
, delimiter

### usage
u=     :  assign User 'blank', results in no permissions
u=r    :  assign user Read (only)
u+wx   :  Add Write and Excute to the User.
u-x    :  Remove Excute from User
a=r    :  assign Read (only) to All Owners

### cli examples
$ chmod u=rwx foo.txt        // User has full permissions
$ chmod u-wx foo.txt         // User now only has Read permissions
$ chmod u-r,g+w,o= foo.txt   // Remove Read from User, Add Write to Group, Set NO permissions to World
```